rol = lambda val, r_bits, max_bits: \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))

def Q(b):    
    return b ^ (rol(b, 17, 32))

def H(message) -> int:

    laststep = True 
    if len(message) == 0:
        laststep = False

    message = list(message)

    S = int("524f464c", 16)
    
    for i in range(0, len(message)):
        message[i] = hex(ord(message[i]))[2:]

    if len(message) % 4 != 0 or len(message) == 0:
        remainder = -((len(message) % 4) - 4);
        for i in range(0, remainder):
            message.append("FF")
    
    print("Message: ")
    print(message)

    chunks = []

    for i in range(0, len(message), 4):
        chunk = "".join(message[i:i+4])
        chunks.append(chunk)
    
    print("Chunks: ") 
    print(chunks)

    for P in chunks:
        print("P: ")
        print(P)
        S = Q(S ^ int(P, 16))
        print("S: ")
        print(hex(S))

    if laststep:
        S = Q(S)
    
    print("Final")
    print(hex(S))

    return S

def test():
    assert Q(int("524f464c", 16)) == int("ded7e2d2", 16)
    assert Q(Q(int("524f464c", 16))) == int("1b725f7d", 16)
    assert Q(Q(Q(int("524f464c", 16)))) == int("a5886999", 16)
    assert H("") == int("ded7e2d2", 16)
    assert H("A") == int("5d725f7f", 16)
    assert H("AB") == int("5f3b5f7f", 16)
    assert H("ABC") == int("5f39137f", 16)
    assert H("ABCD") == int("5f391128", 16)
    assert H("ABCDE") == int("2f69af58", 16)
 
#test()

H(input("Input: "))