rol = lambda val, r_bits, max_bits: \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))

def Q(b):    
    return b ^ (rol(b, 17, 32))

def H(message) -> int:
    laststep = True 
    if len(message) == 0:
        laststep = False

    message = bytearray(message.encode())

    print(message)

    S = bytearray.fromhex("0x524f464c")
    
    #for i in range(0, len(message)):
    #    message[i] = hex(ord(message[i]))[2:]

    if len(message) % 4 != 0 or len(message) == 0:
        remainder = -((len(message) % 4) - 4)
        for i in range(0, remainder):
            message.append(0xFF)
    
    print("Message: ")
    print(message)

    chunks = []

    for i in range(0, len(message), 4):
        chunk = message[i:i+4]
        chunks.append(chunk)
    
    print("Chunks: ") 
    print(chunks)

    for P in chunks:
        S = bytearray(Q(int.from_bytes(S) ^ int.from_bytes(P)))
        #print("S: ")
        #print(hex(S))

    if laststep:
        S = Q(int.from_bytes(S))
    
    print("Final")
    print(S)

    return S

def bruteforce():
    for i in range(0, 2**32):
        if i  % 10000 == 0:
            print("\033[H\033[J")
            print(i)
        hash = H(str(i) + "ABCD")
        if(hash == int("0x632e4e5c", 16)):
            print("Hash found" + hash)
            break

def test():
    assert Q(int("524f464c", 16)) == int("ded7e2d2", 16)
    assert Q(Q(int("524f464c", 16))) == int("1b725f7d", 16)
    assert Q(Q(Q(int("524f464c", 16)))) == int("a5886999", 16)
    assert H("") == int("ded7e2d2", 16)
    assert H("A") == int("5d725f7f", 16)
    assert H("AB") == int("5f3b5f7f", 16)
    assert H("ABC") == int("5f39137f", 16)
    assert H("ABCD") == int("5f391128", 16)
    assert H("ABCDE") == int("2f69af58", 16)
 

#bruteforce()
#test()

H(input("Input: "))

