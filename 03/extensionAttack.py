message = list(input("Input: "))

rol = lambda val, r_bits, max_bits: \
    (val << r_bits%max_bits) & (2**max_bits-1) | \
    ((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))

def Q(b):    
    return b ^ (rol(b, 17, 32))

def H():
    S = int("b0ea6286", 16)
    
    global message
    for i in range(0, len(message)):
        message[i] = hex(ord(message[i]))[2:]

    if len(message) % 4 != 0 or len(message) == 0:
        remainder = -((len(message) % 4) - 4);
        for i in range(0, remainder):
            message.append("FF")
    
    print("Message: ")
    print(message)

    chunks = []

    for i in range(0, len(message), 4):
        chunk = "".join(message[i:i+4])
        chunks.append(chunk)
    
    print("Chunks: ") 
    print(chunks)

    for P in chunks:
        S = Q(S ^ int(P, 16))
        print("S: ")
        print(hex(S))

    S = Q(S)
    print("Final")
    print(hex(S))

H()

