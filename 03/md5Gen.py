import hashlib

message = input("Message: ")

print("\n=====================================================")

hashes = []

count = 0

for i in range(0, 10):
    result = hashlib.md5(bytes((message + str(i)), 'utf-8'))
    if result.hexdigest()[:4] == "0000":
        count = count + 1
        print("Message: "+ (message + str(i)))
        print("Hash: " + result.hexdigest())

print("Count: " + str(count))

print("Expectation: " + str(999999 / 16**4))




