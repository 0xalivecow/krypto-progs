fn red(mut polynomial: u64, def_rel: u64, ) -> u64{
    loop {
        let deg: u32 = def_rel.leading_zeros() - polynomial.leading_zeros();
        if deg > 0 {
           polynomial = polynomial ^ def_rel << deg;
        }
        else {
            polynomial = polynomial ^ def_rel << deg;
            break;
        }
    }

    polynomial
}

fn main() {
    println!("{:x}", red(0x100001, 0x1053));
}

