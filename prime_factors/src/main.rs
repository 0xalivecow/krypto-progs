use num::{Integer, FromPrimitive};

use sqrmult_lib::*;
use num_bigint::BigUint;
use std::str::FromStr;

pub fn ext_euclid(a: i64, b: i64) -> (i64, i64, i64){
    if a == 0{
        return (b, 0, 1);
    }

    let (gcd, x1, y1) = ext_euclid(b % a, a);

    let x = y1 - (b/a) * x1;
    let y = x1;

    println!("{:?}", (gcd, x, y));

    return (gcd, x, y);
}

fn prime_factors(mut num: i64) -> Vec<i64>{
    let mut i = 3;
    let mut pq = vec![];
    while i*i <= num {
        while num % i == 0 {
            pq.push(i);
            num/=i;
        }
            i+=2;
    }

    pq.push(num);

    return pq;

}

fn main() {
    let pq = prime_factors(3822016834723);
    let N = 3822016834723;
    let y:u64 = 2830193791865;
    let phi: i64 = (pq[0] - 1) * (pq[1] - 1);
    let e = 13;
    let mut d = ext_euclid(phi, e);
    println!("{:?}", phi);
    println!("{:?}", pq);
    while d.2 < 0{
        d.2 = (d.2 + phi) % phi
    }
    println!("{:?}", (d.2 % phi));
    println!("{:?}", sq_mult(to_bin(BigUint::from_i64(d.2).unwrap()), BigUint::from_u64(y).unwrap(), BigUint::from_u64(N).unwrap()));
}

