import math
import hashlib



# Parameters
prime = 0x00ffffffff00000001000000000000000000000000ffffffffffffffffffffffff

a = 0x00ffffffff00000001000000000000000000000000fffffffffffffffffffffffc

b = 0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b

gen = 0x046b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c2964fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5

n = 0x00ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551  # Order / q

h = 0x1

asn1_ecc_params = "00048c3aa86d54441a69d3a30c788e5674844ab298253b92f1673e2de534ee98f79d511ef4818b008be75c727ca3dde25c98281b75cea75b6969bd83ebae3e3518f6"

len_point = math.ceil(math.log2(prime)/8)

x = int(asn1_ecc_params[2:len_point], 16)
y = int(asn1_ecc_params[len_point+2:(len_point*2)], 16)

sig_der = 0x304502204854a53830fab30cac49c91b72e7f84d8cb25102db220f6dc7f1a8b31b29b91302210086abf01f3aff9b2a0b3823f2581983a8c38264660ec66bb2f8c648bee88d36e0

# UTILS
def gcdExtended(a, b):
    # Base Case
    if a == 0:
        return b,0,1
             
    gcd,x1,y1 = gcdExtended(b%a, a)
     
    # Update x and y using results of recursive
    # call
    x = y1 - (b//a) * x1
    y = x1
     
    return gcd,x,y


def double_and_add(point, scalar, n, a):
    print(point)
    start = point
    bits = bin(scalar)
    print(bits)
    for i in bits[3:]:
        print(point)
        if i == '1':
            print("Double add")
            point = multiply_points_on_ellip_curve(point, n, a)
            point = add_points_on_ellip_curve(point, start, n)
        else:
            print("double")
            point = multiply_points_on_ellip_curve(point, n, a)
            
    return point
# gcdExtended(n, 2*point[1])[2]
def multiply_points_on_ellip_curve(point, n, a):
    print("In point: " + str(point))
    m = ((3*point[0]**2 + a)*(pow(2*point[1], -1, n))) % n
    u = (m**2 - 2*point[0]) % n
    v = (m**3 - 3*m*point[0] + point[1]) % n
    print("m: " + str(m))
    print("u: " + str(u))
    print("v: " + str(v))

    return (u, ((-v) % n))

def add_points_on_ellip_curve(point1, point2, n):
    print("In point: " + str(point1))
    print("ZÄhler: " + str((point2[1] - point1[1]) % n))
    print("Inverse: " + str(gcdExtended(n, ((point2[0] - point1[0]) % n))[2]))
    m = (((point2[1] - point1[1]) % n) * gcdExtended(n, ((point2[0] - point1[0]) % n))[2]) % n
    r3 = (m**2 - point1[0] - point2[0]) % n
    print("m: " + str(m))
    print("r3: " + str(r3))
    print("m: " + str(m**3 % n))
    print("point1[0]: " + str(point1[0] % n))
    print("2*m*r1: " + str(2*m*point1[0] % n))
    print("m*r2: " + str(m*point2[0] % n))
    print("s1: " + str(point1[1] % n))
    s3 = (m**3 - (2*m*point1[0]) - (m*point2[0]) + point1[1]) % n
    
    return (r3, ((-s3) % n))

"""
   ECDSA-Sig-Value ::= SEQUENCE {
      r     INTEGER,
      s     INTEGER }
      
0:d=0  hl=2 l=  69 cons: SEQUENCE
2:d=1  hl=2 l=  32 prim:  INTEGER           :4854A53830FAB30CAC49C91B72E7F84D8CB25102DB220F6DC7F1A8B31B29B913
36:d=1  hl=2 l=  33 prim:  INTEGER           :86ABF01F3AFF9B2A0B3823F2581983A8C38264660EC66BB2F8C648BEE88D36E0
"""
r = 0x4854A53830FAB30CAC49C91B72E7F84D8CB25102DB220F6DC7F1A8B31B29B913
s = 0x86ABF01F3AFF9B2A0B3823F2581983A8C38264660EC66BB2F8C648BEE88D36E0

# print(x)
# print(y)

print(double_and_add((4, 8), 7, 17, 15))


""" Task two
print("ECDSA Verification: ")
e = int(hashlib.sha256("Kryptologie DHBW Mannheim".encode('utf-8')).hexdigest(), 16)
print(e)

w = gcdExtended(n, s)[2]
print("w: " + str(w))

u1 = (e*w) % n
u2 = (r * w) % n

print(((pow(gen, u1, n) * pow(*m*point[0] + point[1]y, u2, n)) % n).to_bytes(100, 'big').hex())
"""
