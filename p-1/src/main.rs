use std::process::{self, exit};

use sqrmult_lib;
use num::{Integer, FromPrimitive};
use num_bigint::BigUint;

fn is_prime(n: u128) -> bool {
  for i in 2..n{
    if (n%i) == 0{
      return false;
    }
  }
  return true

}

/*
     for p in primes.iter(){
        if p * p > n {
            break;
        }
        if(!(n == 2 || n == 3 || (n+1 % 6 == 0 && n-1 % 6 == 0))){
            return false;
        }
        if n % p == 0 {
            return false;
        }
    }

    return true;
 */

fn ge_prime_array() -> Vec<u128>{
    let mut primes: Vec<u128> = vec![];
    for i in 2..100000{
        if is_prime(i){
            primes.push(i);
        }
    }

    return primes;
}

fn pm1(m: u64) -> u128 {

    let primes = ge_prime_array();

    println!("{:?}", primes);

    let mut k: u128 = 1;
    let mut a: u32 = 1;
    // Berechnen von k
    for i in 2..primes.len() {
        let mut count = 1;
        for j in 0..primes.len() {
            loop {
                // Ist Primzahl hoch count kleiner als die ausgewählte Primzahlt, wird count erhöht 
                if primes[j].pow(count) <= primes[i] {
                    count += 1;
                } 
                // Ist die Primzahl größer als die Ausgewählte, wird count nicht erhöht
                else if primes[j] > primes[i] {
                    count = 1;
                    break;
                } 
                // Ist die Primzahl hoch count größer als die Ausgewählte, wird diese mit der um 1 kleineren Potenz potenziert und
                // mit k multipliziert.
                else {
                    k = k * primes[j].pow(count - 1);
                    count = 1;
                    break;
                }
            }
        
            // Berechnen des ggt(2^k - 1, m) mit square multiply
            let gcd = Integer::gcd(&(
            // Erste Zahl ggT
                sqrmult_lib::sq_mult(
                // Konvertierung des Exponenten in Binärschreibweise
            sqrmult_lib::to_bin(BigUint::from_u128(k).unwrap()), 
                //  Basis (a). Hier a
                BigUint::from_u32(a).unwrap(),
                // Modul für Square multiply. Hier m
            BigUint::from_u64(m).unwrap()) - BigUint::from_u32(1).unwrap()),
            // Zweite Zahl ggt 
            &BigUint::from_u64(m).unwrap()
                );
            //println!("gcd = {:?}", gcd); 

            // Wenn ggt nicht gleich m oder 1 ist, endet der Algorithmus
            if gcd == BigUint::from_u32(1).unwrap() {
                
            }
            else if gcd == BigUint::from_u64(m).unwrap() {
                a += 1;
            }
            else
 {
                println!("gcd = {:?}", gcd); 
                println!("cofactor = {:?}", BigUint::from_u64(m).unwrap() / gcd);
                exit(0);
            }
        }
        if i == primes.len(){
            println!("Exhausted");
            exit(0);
        }
    }
    //println!("{}", k);

    k
}

fn main() {
    pm1(835380);
}
