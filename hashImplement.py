def calcHash():
    message = list(input("Input: "));

    accu = int("a5a5a5a55a5a5a5a55555555aaaaaaaa", 16)

    for i in range(0, len(message)):
        message[i] = hex(ord(message[i]))[2:]

    if len(message) % 16 != 0:
        remainder = -((len(message) % 16) - 16);
        for i in range(0, remainder):
            message.append("FF")

    chunks = []

    for i in range(0, len(message), 16):
        chunk = "".join(message[i:i+16])
        chunks.append(chunk)

    for B in chunks:
        print(hex(accu))
        accu = accu ^ int(B, 16)
    
    print(chunks)
    print(hex(accu))
    return accu

calcHash()

'''
def bruteForce():
    for i in range(0, 340282366920938463463374607431768211456):
        if calcHash(str(int(hex(i)[2:], 16))) == int("316028048703657708663015175772311999829", 16)::
            print(int(i, 16))
            break

bruteForce()
#print(message)

'''
