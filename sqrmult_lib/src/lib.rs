use num_bigint::BigUint;
use std::str::FromStr;

// Konvertierung von Integer in Binärrepresentation mit true = 1 und false = 0
// Representation ist eine Art Array
pub fn to_bin(num: BigUint) -> Vec<bool> {
    let mut bin: Vec<bool> = Vec::new();
    let mut quotient: BigUint = num;
    let mut remainder: BigUint = BigUint::from_str("0").unwrap();
    loop {
        (quotient, remainder) = (
            (&quotient / BigUint::from_str("2").unwrap()),
            &quotient % BigUint::from_str("2").unwrap(),
        );
        //println!("{:?}", quotient);
        if remainder == BigUint::from_str("1").unwrap() {
            bin.push(true);
        } else {
            bin.push(false);
        }
        if quotient == BigUint::from_str("0").unwrap() {
            break;
        }
    }

    // Umkehren der Binärrepresentation und entfernen der ersten Elementes
    bin = bin.into_iter().rev().collect();
    bin.remove(0);

    bin
}

pub fn sq_mult(bin_num: Vec<bool>, mut base: BigUint, modulo: BigUint) -> BigUint {
    let num = base.clone();
    //println!("{:?}", bin_num);
    // Wenn der Wert im Array false ist
    for d in bin_num {
        // Fall für false (0)
        if !d {
            base = base.pow(2) % &modulo;
            #[cfg(debug_assertions)]
            println!("Square: {:?}", base);
        }
        // Fall für true (1)
        else {
            base = (base.pow(2) * &num) % &modulo;
            #[cfg(debug_assertions)]
            println!("Square-multiply: {:?}", base);
        }
    }

    return base;
}
