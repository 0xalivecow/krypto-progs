use clap::Parser;

#[derive(Parser)]
struct Cli {
    /// Known message
    #[arg(short = 'm', long = "message", required = false)]
    message: Option<String>,

    /// Brute force mode
    #[arg(short = 'b', long = "brute_force", required = false, requires_all = ["message", "mic"])]
    brute: bool,

    /// Data to append in ext-mode
    #[arg(short = 'a', long = "append", required = false)]
    append: Option<String>,

    /// Known valid MIC
    #[arg(short = 'k', long = "known-mic", required = false)]
    mic: Option<String>,

    ///Normal hashing mode
    #[arg(short = 'n', long = "normal-hasing", required = false)]
    hasing: bool,

    ///State to extend from
    #[arg(short = 'j', long = "jump-point", required = false)]
    jumppoint: Option<String>,

    ///Hash extension mode
    #[arg(short = 'e', long = "ext-attack", required = false)]
    extend: bool,

}


fn input_handle(input: &str) -> (Vec<Vec<u8>>, bool){
    let mut input_vec: Vec<u8> = input.trim().as_bytes().to_vec();  
    
    chunking(&mut input_vec)
}

fn chunking(input_vec: &mut Vec<u8>) -> (Vec<Vec<u8>>, bool){

    //println!("{:?}", input_vec.len());

    // Check if input is empty
    let lstep: bool;
    if input_vec.len() == 0 {
        lstep = false;
        //println!("{:?}", lstep)
    }else {
        lstep = true;
    }

    //let mut input_vec: Vec<u8> = input.trim().as_bytes().to_vec();  
    
    //println!("{:?}", input_vec);

    // pad input if length is less that k * 4
    if input_vec.len() % 4 != 0 || input_vec.len() == 0{
        let remainder: i32 = -((input_vec.len() as i32 % 4) - 4);
        for _i in 0..remainder{
            input_vec.push(255);
        } 
    }
    
    //println!("{:x?}", input_vec);

    // split up input into 4 byte large chunks of the byte representations of the characters
    let chunks: Vec<Vec<u8>> = input_vec.chunks(4).map(|s| s.into()).collect();

    //println!("Chunks: {:?}", chunks);

    (chunks, lstep)
}

fn Q(number: u32) -> u32{
    return number ^ bit_rotate(number);
}


fn h((chunks, last_step): (Vec<Vec<u8>>, bool), init: u32) -> (u32, u32){
    let mut S: u32 = init;
    //println!("S {:x?}", S);
    let mut count: i32 = 0;
    for chunk in chunks {
        //println!("{:?}", chunk);
        // convert values in vector (somewhat an array list in rust) into array with defined size for conversion into 32 bit integer.
        // Values of vector are 8 bit, we need a 32 bit number
        let chunk2: [u8; 4] = chunk[0..4].try_into().unwrap();
        //println!("S in {:x?}", S);
        //println!("C in {:x?}", u32::from_be_bytes(chunk2));
        //println!("S ^ C in {:x?}", S ^ u32::from_be_bytes(chunk2));

        // convert array of 8 bit numbers to 32 bit integer and perform calculations. Conversion is nececary for successful bit rotation, as this needs to
        // rotate the full 32 bit number.
        S = Q(S ^ u32::from_be_bytes(chunk2));
        count += 1;
        if count == 4 {
            count = 0;
        }
        //println!("{:x?}", S)
    }

    
    //println!("\x1b[H");
    //println!("\x1b[2J");

    // last step of algorithm
    let before_last = S.clone();
    // Don't perform last step if input is empty
    if last_step {       
        //println!("{:?}", last_step);
        S = Q(S); 
    }
    
    //println!("Final Hash: {:x?}", S);
    (S, before_last)
}

fn bit_rotate(number: u32) -> u32{
    // roteate number felft by 17 bit
    number.rotate_left(17)
}

fn bruteforce(init_mic: u32, message: &str) -> (i64, u32, u32){
    let mut found:i64 = 0;
    let mut fin_hash: u32 = 0;
    let mut before_last: u32= 0;
    println!("MIC to find: {:x?}", init_mic);
    for i in 1..=4294967296i64 {
        //println!("{:?}", msg);
        
        //println!("{:x?}", br);

        // Limit printing for performance reasons
        if i % 1000000 == 0 {
            println!("\x1b[H");
            println!("Running: {:?}", i);
        }
        
        let message: Vec<u8> = message.as_bytes().to_vec();
        let mut br = i.to_be_bytes().to_vec();

        // Append bruteforce value and remove leading zeros
        br.extend(message);
        br.retain(|&x| x != 0u8);
        // Check if bruteforced value is equal to knwon mic
        (fin_hash, before_last) = h(chunking(&mut br),0x524f464c);
        if fin_hash == init_mic {
            found = i;
            break;
        }
        
        /*
        thread::spawn(move || {
            let message: Vec<u8> = "AAAAaa".as_bytes().to_vec();
            let mut br = j.to_be_bytes().to_vec();
            br.extend(message);
            br.retain(|&x| x != 0u8);
            if h(chunking(&mut br)) == init_mic {
                found = i;
                break;
            }
        });
        */
    }
    println!("\x1b[H");
    println!("\x1b[2J");
    println!("Found: {:x?}", found);
    println!("Before last: {:x?}", before_last);
    println!("Last: {:x?}", fin_hash);

    // Return final and before final, used in case bruteforce was successful, value for later use
    (found, fin_hash, before_last)
}

fn hash_extension(jumppad: u32, extension_msg: &str){

    /* POC input
    let mut key: Vec<u8> = hex::decode("289488ae6d71c82da1502c0130ec04e0").unwrap();
    key.extend("AAAAaa".as_bytes().to_vec());
    let init_mic: u32 = h(chunking(&mut key),0x524f464c).0;
    */
    //====================================================================
    // Bruteforce

    //let mut new_init:u32 = 0;

    //let new_init = 0xccd1d7ff;
    
    let extension_message = extension_msg;
    //key.extend(extension_message.as_bytes().to_vec()); POC
    let extension_hash = h(input_handle(extension_message), jumppad);
    //let verified_msg = h(chunking(&mut key), 0x524f464c);
    println!("Extension hash: {:x?}", extension_hash.0);

}

fn main() {
    println!("\x1b[H");
    println!("\x1b[2J");
    let args = Cli::parse();
    if args.extend {
        hash_extension(u32::from_str_radix(&args.jumppoint.unwrap(), 16).unwrap(), &args.append.unwrap());
    }
    else if args.hasing {
        println!("{:x?}", h(input_handle(&args.message.unwrap()), 0x524f464c).0);
    }
    else if args.brute {
        //let (found, fin_hash, before_last) = 
        println!("{:x?}", bruteforce(u32::from_str_radix(&args.mic.unwrap(), 16).unwrap(), &args.message.unwrap()).2);
        //init_mic = before_last;
    }
    else {
        println!("No arguments provided")
    }
    //println!("Final Hash: {:x?}", h(input_handle("A")));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn qs() {
        assert_eq!(Q(0x524f464c), 0xded7e2d2);
    }

    #[test]
    fn qqs() {
        assert_eq!(Q(Q(0x524f464c)), 0x1b725f7d);
    }

    #[test]
    fn qqqs() {
        assert_eq!(Q(Q(Q(0x524f464c))), 0xa5886999);
    }

    #[test]
    fn empty() {
        assert_eq!(h(input_handle(""), 0x524f464c).0, 0xded7e2d2);
    }

    #[test]
    fn a(){
        assert_eq!(h(input_handle("A"), 0x524f464c).0, 0x5d725f7f);
    }

    #[test]
    fn ab(){
        assert_eq!(h(input_handle("AB"), 0x524f464c).0, 0x5f3b5f7f);
    }

    #[test]
    fn abc(){
        assert_eq!(h(input_handle("ABC"), 0x524f464c).0, 0x5f39137f);
    }

    #[test]
    fn abcd(){
        assert_eq!(h(input_handle("ABCD"), 0x524f464c).0, 0x5f391128);
    }

    /* 
    #[test]
    fn brute() {
        let message: Vec<u8> = "Hello".as_bytes().to_vec();
        let mut br: Vec<u8> = 65u64.to_be_bytes().to_vec();
        br.extend(message);
        br.retain(|&x| x != 0u8);
        //println!("{:x?}", br);
        assert_eq!(h(chunking(&mut br), 0x524f464c), h(input_handle("AHello"), 0x524f464c))
    }
    */

}
