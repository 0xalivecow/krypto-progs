import json

with open('Teil2_2_chifrat', 'r') as f:
    text = f.read().split()

#print(text)

dataTotal = {}
for d in text:
    if not d in dataTotal:
        dataTotal[d] = 1
    else:
        dataTotal[d] += 1

print(dataTotal)

dataPercent = {}
for d in dataTotal:
    dataPercent[d] = round((dataTotal[d] / (len(text)) * 100), 2)

json_object1 = json.dumps(dict(sorted(dataTotal.items(), key= lambda x:x[1])), indent=4)
print(json_object1)

json_object2 = json.dumps(dict(sorted(dataPercent.items(), key= lambda x:x[1])), indent=4)
print(json_object2)

