use num::{BigInt, Num, ToPrimitive};
//use rand_chacha::ChaCha20Rng;
use rand::Rng;
use sha2::{Digest, Sha256};

fn mgf1(seed: &[u8], mask_len: usize) -> Vec<u8> {
    let mut mask = Vec::new();
    let mut counter = 0u32;
    let mut rng = rand::thread_rng();

    while mask.len() < mask_len {
        let mut sha256 = Sha256::new();
        sha256.update(seed);
        sha256.update(&counter.to_be_bytes());
        let hash = sha256.finalize();

        for byte in hash.iter() {
            mask.push(*byte);
            if mask.len() == mask_len {
                break;
            }
        }

        counter += 1;
    }

    mask
}

fn rsa_oaep(){
    let n = BigInt::from_str_radix("AF5466C26A6B662AC98C06023501C9DF6036B065BD1F6804B1FC86307718DA4048211FD68A06917DE6F81DC018DCAF84B38AB77A6538BA2FE6664D3FB81E4A0886BBCDAB071AD6823FE20DF1CD67D33FB6CC5DA519F69B11F3D48534074A83F03A5A9545427720A30A27432E94970155A026572E358072023061AF65A2A18E85", 16).unwrap();
    
    let e = 65537;
    
    //let seed = ChaCha20Rng::get_seed();
    let seed: u32 = 0x4d15f73f74996ee5;
    
    println!("{:?}", n);
}

fn main() {
    mgf1();
    println!("Hello, world!");
}

