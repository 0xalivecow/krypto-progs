pre_p_box = 0b11110000

outval = ((pre_p_box & 0x54) >> 2) | (pre_p_box & 0xAA) | ((pre_p_box & 0x1) << 6)

print("{:08b}".format(outval))

