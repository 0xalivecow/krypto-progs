use clap::Parser;

#[derive(Parser)]
struct Cli {
    /// Known message
    #[arg(short = 'a', long = "a-num")]
    a: i64,

    #[arg(short = 'b', long = "b-num")]
    b: i64,
}

pub fn ext_euclid(a: i64, b: i64) -> (i64, i64, i64){
    if a == 0{
        return (b, 0, 1);
    }

    let (gcd, x1, y1) = ext_euclid(b % a, a);

    let x = y1 - (b/a) * x1;
    let y = x1;

    println!("{:?}", (gcd, x, y));

    return (gcd, x, y);
}

fn main() {
    let args = Cli::parse();
    let (gcd, x, mut y) = ext_euclid(args.a, args.b);

    println!("{:?}", (gcd, x, y));
}
