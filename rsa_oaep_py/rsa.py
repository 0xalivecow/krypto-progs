import hashlib
import random

def mgf1(seed: bytes, length: int, hash_func=hashlib.sha256) -> bytes:
    hLen = hash_func().digest_size
    # https://www.ietf.org/rfc/rfc2437.txt
    # 1.If l > 2^32(hLen), output "mask too long" and stop.
    if length > (hLen << 32):
        raise ValueError("mask too long")
    # 2.Let T  be the empty octet string.
    T = b""
    # 3.For counter from 0 to \lceil{l / hLen}\rceil-1, do the following:
    # Note: \lceil{l / hLen}\rceil-1 is the number of iterations needed,
    #       but it's easier to check if we have reached the desired length.
    counter = 0
    while len(T) < length:
        # a.Convert counter to an octet string C of length 4 with the primitive I2OSP: C = I2OSP (counter, 4)
        C = int.to_bytes(counter, 4, 'big')
        # b.Concatenate the hash of the seed Z and C to the octet string T: T = T || Hash (Z || C)
        T += hash_func(seed + C).digest()
        counter += 1
    # 4.Output the leading l octets of T as the octet string mask.
    return T[:length]

def rsa_oaep(seed, message_in, testing):
    n: int = 0xE0CB40E9C1D4D03622FECFF41BC938971A2A33EF4CE5AF51B9EBF64E75731AEDFCE4FA025EB0CE3605A383C74FC99B6D12AA71324D86D2A2AED1F699EF073A07C7BB8AC6E836798D6E1B8AAC7099107BADF0490231CC891DB12D8E1B95F1AD7EA7FF852B7741DA5FEC3260A9F6C2AF8A6E76B9CF6620E28C9CC8D3217B04E6CD
    
    #0x00AF5466C26A6B662AC98C06023501C9DF6036B065BD1F6804B1FC86307718DA4048211FD68A06917DE6F81DC018DCAF84B38AB77A6538BA2FE6664D3FB81E4A0886BBCDAB071AD6823FE20DF1CD67D33FB6CC5DA519F69B11F3D48534074A83F03A5A9545427720A30A27432E94970155A026572E358072023061AF65A2A18E85
    
    modulus_len: int = (n.bit_length()) // 8
    #print(modulus_len)
    
    if isinstance(message_in, int):
        message = message_in
    else:
        message: int = int(message_in.encode('utf-8').hex(), 16)
    
    message_len: int = int(message.bit_length() // 8)

    lead01: int = 0x01
    
    e = 65537

    seed_len = 8
    #print(seed_len)
    # assert seed_len == 8
    
    len_db = int(modulus_len - 1 - 8)
    #print("Len DB: " + str(len_db))
    # assert len_db == 119
    len_pad = int(len_db - 1 - message_len)
    padding: int = int(("0x" + ("00" * len_pad)), 16)
    tmp1: int = ((lead01 << message.bit_length() + (8 - (message.bit_length() % 8))) | message)
    DB: int = ((padding << tmp1.bit_length() + (8 - (tmp1.bit_length() % 8))) | tmp1)
    #print(DB.bit_length() // 8)
    # print("%0119x" % DB)
    
    #print(seed.bit_length()//8)
    db_Mask = int(mgf1(seed.to_bytes(8, 'big'), len_db).hex(), 16)
    #print("DB Mask: ", str(db_Mask.bit_length()//8))
    # Check masked Seed
    if testing:
        assert db_Mask == 0xea600669f6f16b3a2ad05d4b6d9b23911c8cc432fddd8d34a68d88af3d787b7eebf6cd1b720812086758ce56e24ab819ccd8fb5eedb1cae9f6f895667d7f89d0454b828777ecabc040a649c8956e78ec1c721370663065cbc343deabad9eb6f2aceab6bfed5beb232cc55413bfffa06e68627d7ec3ded5

    masked_db = (db_Mask ^ DB)
    #print("Masked DB: " + str(masked_db.bit_length()//8))
    # Check Mask Data Block
    if testing:
        assert masked_db == 0xea600669f6f16b3a2ad05d4b6d9b23911c8cc432fddd8d34a68d88af3d787b7eebf6cd1b720812086758ce56e24ab819ccd8fb5eedb1cae9f6f895667d7f89d0454b828777ecabc040a649c8956e78ec1c721370663065cbc343deabad9eb6f2aceab6bfed5bea6543aa3672cddf915c5b564848f4e6ec

    seed_mask = int(mgf1(masked_db.to_bytes(len_db, 'big'), seed_len).hex(), 16)
    # Check seed_mask
    if testing:
        assert seed_mask == 0x713162084a4e0e6d
        
    masked_seed = seed_mask ^ seed
    # Check masked seed
    if testing:
        assert masked_seed == 0xdb2040f6425bb082

    oaep = (masked_seed << (masked_db.bit_length()) | masked_db)
    oaep = (0x00 << (oaep.bit_length()) | oaep)
    # print((oaep.to_bytes(128, 'big').hex()))
    # Check oaep
    if testing:
        assert oaep == 0x00db2040f6425bb082ea600669f6f16b3a2ad05d4b6d9b23911c8cc432fddd8d34a68d88af3d787b7eebf6cd1b720812086758ce56e24ab819ccd8fb5eedb1cae9f6f895667d7f89d0454b828777ecabc040a649c8956e78ec1c721370663065cbc343deabad9eb6f2aceab6bfed5bea6543aa3672cddf915c5b564848f4e6ec

    c = pow(oaep, e, n) # Square multiply with modulus n
    # Check c
    if testing:
        assert c == 0x1b57819fa11340ac8b1843c87db7adb126daa8b6dde1feefd7af721cee8f46b6e2c361fc04ac055406a342187388b019dba0bc3f6503f267b848f7cc86b29a3d0b32730ccf04c5a8a3e1255708cbc6a6a648015e30f38b1c1c7aa9d2b0e67a775c7ad1cb72ff76c000af46e7cada3c3b45b5f4d1ec8e0596928cc9b46ee2b53d


    # print(mgf1(masked_db.to_bytes(len_db, 'big'), seed_len))
    return c

print("="*50 + "\n")
print("Testing vectors")
#print(rsa_oaep(0xaa1122fe0815beef, 0x466f6f62617220313233343536373839, True).to_bytes(128, 'big').hex())

print("="*50 + "\n")
print("real run")
print(rsa_oaep(random.getrandbits(64), input(), False).to_bytes(128, 'big').hex())
print("="*50 + "\n")


#print(rsa_oaep(0xaa1122fe0815beef, "sus", False).to_bytes(128, 'big').hex())
