fn input_handle(inp: u16) -> u16 {
    inp
}

// FUntion to swap bits around
fn swap_bit(num: u16, p1: u16, p2: u16) -> u16 {
    let bit1 = (num >> p1) & 1;
    let bit2 = (num >> p2) & 1;

    let mut x = (bit1 ^ bit2);

    x = ((x << p1) | (x << p2));

    let res = num ^ x;

    res
}

fn feistel_net(mut inp: u16, key: u16) -> u16 {
    let s_box = vec![4, 3, 9, 0xa, 0xb, 2, 0xe, 1, 0xd, 0xc, 8, 6, 7, 5, 0, 0xf];

    #[cfg(debug_assertions)]
    println!("initial: {:16b}", inp);

    //Swap the outermost four bits
    //ex: 1xxxxxxx0 -> 0xxxxxxx1
    inp = swap_bit(inp, 0, 15);
    inp = swap_bit(inp, 1, 14);
    inp = swap_bit(inp, 2, 13);
    inp = swap_bit(inp, 3, 12);

    #[cfg(debug_assertions)]
    println!("Swapped: {:16b}", inp);

    // Get first block to go into sbox via masking and shift
    let sbox_op_one = (inp & 0xF0);
    #[cfg(debug_assertions)]
    println!("{:16b}", (sbox_op_one >> 4));
    // Clear bit range where first sbox transformation will be written
    inp = inp & !sbox_op_one;

    #[cfg(debug_assertions)]
    println!("Sbox value: {:b}", s_box[(sbox_op_one >> 4) as usize]);
    // Write sbox transformed block 
    inp = inp ^ (s_box[(sbox_op_one >> 4) as usize] << 4);

    #[cfg(debug_assertions)]
    println!("First sBox: {:16b}", inp);
    let sbox_op_two = (inp & 0xF00);
    inp = inp & !sbox_op_two;
    inp = inp ^ (s_box[(sbox_op_two >> 8) as usize] << 8);

    // XOR with key
    inp ^ key
}

fn multi_run(mut inp: u32, round_keys: &Vec<u16>) -> Vec<u16> {
    #[cfg(debug_assertions)]
    println!("{:032b}", inp);

    // Split 32bit block into two 16bit via masking and shift
    let b1 = (inp >> 16) as u16;
    let b2 = (inp & 0xFFFF) as u16;

    #[cfg(debug_assertions)]
    println!("{:x}, {:x}", b1, b2);

    let mut blocks = vec![b1, b2];

    // Run F funtion for all keys
    for i in 0..=2 {
        let left = blocks[1];
        blocks[1] = blocks[0] ^ feistel_net(blocks[1], round_keys[i]);
        blocks[0] = left;
    }

    #[cfg(debug_assertions)]
    println!("{:?}", blocks);
    blocks
}

fn main() {
    let mut keys = vec![0xdead, 0xc0ff, 0xee5a];
    println!("Encrypt abcd0815: {:04x?}", multi_run(0xabcd0815, &keys));

    keys.reverse();
    let mut tmp = multi_run(0xb479c5c1, &keys);
    tmp.reverse();
    println!("Decrypt c5c1b479: {:04x?}", tmp);

    tmp = multi_run(0x56781234, &keys);
    tmp.reverse();
    println!("Decrypt 12345678: {:04x?}", tmp);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_1234_0000() {
        assert_eq!(feistel_net(0x1234, 0x0000), 0x29a8);
    }

    #[test]
    fn t_1234_2345() {
        assert_eq!(feistel_net(0x1234, 0x2345), 0x0aed);
    }

    #[test]
    fn t_abcd_beef() {
        assert_eq!(feistel_net(0xabcd, 0xbeef), 0x089a);
    }

    #[test]
    fn t_9876_fedc() {
        assert_eq!(feistel_net(0x9876, 0xfedc), 0x93c5);
    }

    #[test]
    fn tripple_round() {
        assert_eq!(
            multi_run(0x12345678, &vec![0x1aa2, 0x2bb3, 0x3cc4]),
            vec![0x4313, 0xe07a]
        );
    }
}
