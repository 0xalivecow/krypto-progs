file1 = open("Teil2_2_chifrat", "w")

#from spellchecker import SpellChecker

with open('Teil2_2_chifrat', 'r') as f:
    text = f.read().split()

#print(text)
for i in range(0, 128):
	out = ""
	print("\n ================================================")
	print("Key: " + str(i))
	for c in text:
		val = hex(int("0x" + c, 16) ^ int(hex(i), 16) % 128)[2:]
		out = chr(int(val, 16))	
		print(repr(out).strip("'"), end='')
	print("\n ================================================")
